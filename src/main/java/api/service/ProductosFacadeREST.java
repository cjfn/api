/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.service;

import api.Productos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author cjfn
 */
@Stateless
@Path("api.productos")
public class ProductosFacadeREST extends AbstractFacade<Productos> {// extiende patron abstact facade de objeto

    @PersistenceContext(unitName = "com.javahelps_restapi_war_1.0-SNAPSHOTPU")
    private EntityManager em;
// crea clase super de objeto
    public ProductosFacadeREST() {
        super(Productos.class);
    }
// crea nueva entidad de objeto
    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Productos entity) {
        super.create(entity);
    }
// modifica entidad
    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Productos entity) {
        super.edit(entity);
    }
// elimina entidad
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }
// busqueda por id
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Productos find(@PathParam("id") Integer id) {
        return super.find(id);
    }
// listado de todos los productos
    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Productos> findAll() {
        return super.findAll();
    }
// busqueda entre rangos
    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Productos> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }
// conteo de todos los productos
    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
// retorno de entidad
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
