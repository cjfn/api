  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cjfn
 */
@Entity
@Table(name = "productos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Productos.findAll", query = "SELECT p FROM Productos p")
    , @NamedQuery(name = "Productos.findById", query = "SELECT p FROM Productos p WHERE p.id = :id")
    , @NamedQuery(name = "Productos.findByCodigoProducto", query = "SELECT p FROM Productos p WHERE p.codigoProducto = :codigoProducto")
    , @NamedQuery(name = "Productos.findByAplicaDescuento", query = "SELECT p FROM Productos p WHERE p.aplicaDescuento = :aplicaDescuento")
    , @NamedQuery(name = "Productos.findByPorcentajeDescuento", query = "SELECT p FROM Productos p WHERE p.porcentajeDescuento = :porcentajeDescuento")
    , @NamedQuery(name = "Productos.findByNombre", query = "SELECT p FROM Productos p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Productos.findByPrecioVenta", query = "SELECT p FROM Productos p WHERE p.precioVenta = :precioVenta")
    , @NamedQuery(name = "Productos.findByPrecioCompra", query = "SELECT p FROM Productos p WHERE p.precioCompra = :precioCompra")
    , @NamedQuery(name = "Productos.findByExistencia", query = "SELECT p FROM Productos p WHERE p.existencia = :existencia")
    , @NamedQuery(name = "Productos.findByFactura", query = "SELECT p FROM Productos p WHERE p.factura = :factura")
    , @NamedQuery(name = "Productos.findByProveedorId", query = "SELECT p FROM Productos p WHERE p.proveedorId = :proveedorId")
    , @NamedQuery(name = "Productos.findByFechaCreated", query = "SELECT p FROM Productos p WHERE p.fechaCreated = :fechaCreated")
    , @NamedQuery(name = "Productos.findByFechaUpdated", query = "SELECT p FROM Productos p WHERE p.fechaUpdated = :fechaUpdated")
    , @NamedQuery(name = "Productos.findBySucursal", query = "SELECT p FROM Productos p WHERE p.sucursal = :sucursal")
    , @NamedQuery(name = "Productos.findByObservaciones", query = "SELECT p FROM Productos p WHERE p.observaciones = :observaciones")
    , @NamedQuery(name = "Productos.findByUsuario", query = "SELECT p FROM Productos p WHERE p.usuario = :usuario")
    , @NamedQuery(name = "Productos.findByIdunico", query = "SELECT p FROM Productos p WHERE p.idunico = :idunico")
    , @NamedQuery(name = "Productos.findByActivo", query = "SELECT p FROM Productos p WHERE p.activo = :activo")})
public class Productos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "codigo_producto")
    private String codigoProducto;
    @Column(name = "aplica_descuento")
    private Boolean aplicaDescuento;
    @Column(name = "porcentaje_descuento")
    private Integer porcentajeDescuento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre")
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_venta")
    private BigDecimal precioVenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio_compra")
    private BigDecimal precioCompra;
    @Column(name = "existencia")
    private Integer existencia;
    @Column(name = "factura")
    private Integer factura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "proveedor_id")
    private int proveedorId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreated;
    @Column(name = "fecha_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaUpdated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sucursal")
    private int sucursal;
    @Size(max = 100)
    @Column(name = "observaciones")
    private String observaciones;
    @Size(max = 50)
    @Column(name = "usuario")
    private String usuario;
    @Size(max = 50)
    @Column(name = "idunico")
    private String idunico;
    @Column(name = "activo")
    private Integer activo;

    public Productos() {
    }

    public Productos(Integer id) {
        this.id = id;
    }

    public Productos(Integer id, String codigoProducto, String nombre, BigDecimal precioVenta, BigDecimal precioCompra, int proveedorId, Date fechaCreated, int sucursal) {
        this.id = id;
        this.codigoProducto = codigoProducto;
        this.nombre = nombre;
        this.precioVenta = precioVenta;
        this.precioCompra = precioCompra;
        this.proveedorId = proveedorId;
        this.fechaCreated = fechaCreated;
        this.sucursal = sucursal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public Boolean getAplicaDescuento() {
        return aplicaDescuento;
    }

    public void setAplicaDescuento(Boolean aplicaDescuento) {
        this.aplicaDescuento = aplicaDescuento;
    }

    public Integer getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(Integer porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(BigDecimal precioVenta) {
        this.precioVenta = precioVenta;
    }

    public BigDecimal getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(BigDecimal precioCompra) {
        this.precioCompra = precioCompra;
    }

    public Integer getExistencia() {
        return existencia;
    }

    public void setExistencia(Integer existencia) {
        this.existencia = existencia;
    }

    public Integer getFactura() {
        return factura;
    }

    public void setFactura(Integer factura) {
        this.factura = factura;
    }

    public int getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(int proveedorId) {
        this.proveedorId = proveedorId;
    }

    public Date getFechaCreated() {
        return fechaCreated;
    }

    public void setFechaCreated(Date fechaCreated) {
        this.fechaCreated = fechaCreated;
    }

    public Date getFechaUpdated() {
        return fechaUpdated;
    }

    public void setFechaUpdated(Date fechaUpdated) {
        this.fechaUpdated = fechaUpdated;
    }

    public int getSucursal() {
        return sucursal;
    }

    public void setSucursal(int sucursal) {
        this.sucursal = sucursal;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getIdunico() {
        return idunico;
    }

    public void setIdunico(String idunico) {
        this.idunico = idunico;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Productos)) {
            return false;
        }
        Productos other = (Productos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "api.Productos[ id=" + id + " ]";
    }
    
}
